package org.example;

import java.util.Arrays;

/**
 * /**
 *  * SortingApp is a small Java application that sorts integer
 *  values provided as command-line arguments.
 *  ------------------------------------------------------------
 * This simple command line app sorts all numeric
 * arguments in ascending order
 * ------------------------------------------------------------
 * Input looks like:
 * 6 1 4 2 5 3 ....
 * pay attention that input arguments separated by spaces
 * ------------------------------------------------------------
 * Output looks like:
 * Sorted numbers: [1, 2, 3, 4, 5, 6, ...]
 *------------------------------------------------------------
 */
public class Main {
    public static String sortedInput; // for testing purpose only!
    /**
     * Main method to execute the sorting functionality.
     *
     * @param args Command-line arguments containing integer values to be sorted.
     */

    public static void main( String[] args ) {

        if (args.length == 0) {
            System.out.println("No arguments provided.");
            sortedInput = "No arguments provided."; // for testing purpose only!
            return;
        }

        int[] numbers = Arrays.stream(args)
                .mapToInt(Integer::parseInt)
                .toArray();
        Arrays.sort(numbers);
        sortedInput = "Sorted numbers: " + Arrays.toString(numbers); // for testing purpose only!
        System.out.println("Sorted numbers: " + Arrays.toString(numbers));
    }
    public static String getSortedInput(){ // for testing purpose only!
        return sortedInput;
    }
}
