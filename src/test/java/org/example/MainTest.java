package org.example;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
@RunWith(Parameterized.class)
public class MainTest {
    private final String[] args;
    private final String expectedOutput;


    /**
     * Create the test case
     *
     * @param args name of the test case
     */

    public MainTest(String[] args, String expectedOutput ){
        this.args = args;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, "No arguments provided."},
                {new String[]{"4"}, "Sorted numbers: [4]"},
                {new String[]{"87", "78", "65", "3", "1", "2", "0", "1", "333", "111"},
                        "Sorted numbers: [0, 1, 1, 2, 3, 65, 78, 87, 111, 333]"},
                {new String[]{"4", "9", "5", "0", "3", "2", "10", "1", "6", "8", "7", "87", "13", "17"},
                        "Sorted numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 13, 17, 87]"}
        });
    }

    @Test
    public void testSorting(){

        Main.main(args);

        assertEquals(expectedOutput, Main.getSortedInput());
    }

}
